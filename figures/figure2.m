figure
legend('Gliding-Box Lacunarity', 'Tug-Of-War Lacunarity')

%%
subplot(3,2,1)
load('../results/mengercarpet2.mat')
mean_tow_lac = mean(log10(tow_lac(:,:))); 
stddev       = std(log10(tow_lac(:,:)));   

hold on
scatter(log10(width),log10(gb_lac),'x')
errorbar(log10(width),mean_tow_lac,stddev,'o')
legend('Gliding-Box Lacunarity', 'Tug-Of-War Lacunarity','Location','northoutside'...
    ,'Orientation','horizontal','Position',[0.9,0.9,450,610])
xlabel('log r')
ylabel('log \Lambda(r)')
axis([-inf inf -inf 0.801])
hold off

%%
subplot(3,2,2)
load('../results/mengercarpet3.mat')
mean_tow_lac = mean(log10(tow_lac(:,:)));   
stddev       = std(log10(tow_lac(:,:)));    

hold on
scatter(log10(width),log10(gb_lac),'x')
errorbar(log10(width),mean_tow_lac,stddev,'o')
xlabel('log r')
ylabel('log \Lambda(r)')
axis([-inf inf -inf 0.6])
hold off

%%
subplot(3,2,3)
load('../results/mengercarpet4.mat')
mean_tow_lac = mean(log10(tow_lac(:,:)));  
stddev       = std(log10(tow_lac(:,:)));    

hold on
scatter(log10(width),log10(gb_lac),'x')
errorbar(log10(width),mean_tow_lac,stddev,'o')
xlabel('log r')
ylabel('log \Lambda(r)')
axis([-inf inf -inf 0.6])
hold off

%%
subplot(3,2,4)
load('../results/solargranulation.mat')
mean_tow_lac = mean(log10(tow_lac(:,:)));  
stddev       = std(log10(tow_lac(:,:)));   

hold on
scatter(log10(width),log10(gb_lac),'x')
errorbar(log10(width),mean_tow_lac,stddev,'o')
xlabel('log r')
ylabel('log \Lambda(r)')
axis([-inf inf -inf 0.6])
hold off

%%
subplot(3,2,6)
load('../results/filled.mat')
mean_tow_lac = mean(log10(tow_lac(:,:)));  
stddev       = std(log10(tow_lac(:,:)));   

hold on
scatter(log10(width),log10(gb_lac),'x')
errorbar(log10(width),mean_tow_lac,stddev,'o')
xlabel('log r')
ylabel('log \Lambda(r)')
axis([-inf inf -inf 0.6])
hold off

%%
subplot(3,2,5)
load('../results/randmap.mat')
mean_tow_lac = mean(log10(tow_lac(:,:)));  
stddev       = std(log10(tow_lac(:,:)));   

hold on
scatter(log10(width),log10(gb_lac),'x')
errorbar(log10(width),mean_tow_lac,stddev,'o')
xlabel('log r')
ylabel('log \Lambda(r)')
axis([-inf inf -inf 0.6])
hold off

%%
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 [7 6.38]],...
    'PaperSize',[[7 6.38]]);
print -dpdf -painters Figure2