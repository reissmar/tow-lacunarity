figure

%%
load('../results/computationtime.mat')
hold on
plot(size_image,t_gb/1000,'r--', 'Marker','diamond')
plot(size_image,t_tow/1000, 'Marker','square')

xlabel({'Image Size [pixel]'});
ylabel({'Duration [sec]'});
xlim([150,4200])
legend('Gliding-Box Lacunarity',...
    'Tug-Of-War Lacunarity', 'Location','northwest');

%%
set(gca,'XTickLabel',{'256^2','512^2','1024^2','2048^2','4096^2'},...
    'XTick',[256 512 1024 2048 4096])
set(gca,'FontSize',9.5)
set(gca,'YScale','log','YGrid','on')
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 [5.83 4.38]],...
    'PaperSize',[[5.83 4.38]]);
print -dpdf -painters Figure4