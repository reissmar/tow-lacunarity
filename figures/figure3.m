figure
load('../results/lacunaritymeasure.mat')

hold on 
boxplot(lac_meas,label)
scatter(pos,gb_lac,'x')
hold off
set(gca,'XTickLabel',{'Menger Carpet (2)',...
    'Menger Carpet (3)','Menger Carpet (4)','Granulation'},...
    'XTick',[1 2 3 4])
ylabel({'$\bar{\Lambda}$'}, 'Interpreter', 'latex');
text(5,5,'$\bar{s}$', 'FontSize', 20, 'Interpreter', 'latex')
legend('Gliding-Box Lacunarity');

%%
set(gca,'YScale','log','YGrid','on')
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 [7 4.38]],...
    'PaperSize',[[7 4.38]]);
print -dpdf -painters Figure3